package com.company;

public class Products extends Department {

    String deptHead;
    Employee empTable[] = new Employee[3];

    public int numberOfEmployees(){
        return empTable.length;
    }

    public String nameOfDeparment(){
        return("Products");
    }

    public String getHeadOfDept(){
        for(int i=0;i<3;i++){
            if(empTable[i].role.equals("head")){
                return (deptHead=empTable[i].employeeName);
            }
        }
        return("null");
    }

    public  void populatedValues(Employee details){
        Employee employee1=new Employee("Peter","head");
        empTable[1]=employee1;
        Employee employee2=new Employee("Parker","null");
        empTable[2]=employee2;
        Employee employee3=new Employee("Tony","null");
        empTable[3]=employee3;
    }




}
